#!/bin/ash

source /.env
BKUPDIR=/tmp/bkup

echo `date`" : Backup started : "
docker volume prune --force --all
rm -rf ${BKUPDIR}/*

# registryに関するボリュームはキャッシュ領域なので含めない
# ${BKUPVOLUMENAME}は別途マウントするので含めない
EXCEPTVOLUMES=${BKUPVOLUMENAME}'|registry'

# Get Hot-backup
# Mariadb
for x in `docker ps --filter expose=3306 --format "{{.Names}},{{.Mounts}}" --no-trunc`; do
    NAME=`echo $x|cut -d ',' -f1`
    SAVEDIR=${BKUPDIR}/${NAME}_hotbackup
    DBVOL=`echo $x|cut -d ',' -f2-|tr ',' '|'`
    EXCEPTVOLUMES=${EXCEPTVOLUMES}"|${DBVOL}"
    mkdir ${SAVEDIR}
    docker exec ${NAME} sh -c 'mariadb-dump -u root -p${MARIADB_ROOT_PASSWORD} --all-databases' > ${SAVEDIR}/mariadb.sql
done

# PostgreSQL
for x in `docker ps --filter expose=5432 --format "{{.Names}},{{.Mounts}}" --no-trunc`; do
    NAME=`echo $x|cut -d ',' -f1`
    SAVEDIR=${BKUPDIR}/${NAME}_hotbackup
    DBVOL=`echo $x|cut -d ',' -f2-|tr ',' '|'`
    EXCEPTVOLUMES=${EXCEPTVOLUMES}"|${DBVOL}"
    mkdir ${SAVEDIR}
    docker exec ${NAME} sh -c 'pg_dumpall -U ${POSTGRES_USER:-postgres}' > ${SAVEDIR}/postgre.sql
done

# MongoDB
for x in `docker ps --filter expose=27017 --format "{{.Names}},{{.Mounts}}" --no-trunc`; do
    NAME=`echo $x|cut -d ',' -f1`
    SAVEDIR=${BKUPDIR}/${NAME}_hotbackup
    DBVOL=`echo $x|cut -d ',' -f2-|tr ',' '|'`
    EXCEPTVOLUMES=${EXCEPTVOLUMES}"|${DBVOL}"
    mkdir ${SAVEDIR}
    docker exec ${NAME} mongodump --archive > ${SAVEDIR}/mdbdata.db
done

# Redis
for x in `docker ps --filter expose=6379 --format "{{.Names}},{{.Mounts}}" --no-trunc`; do
    NAME=`echo $x|cut -d ',' -f1`
    SAVEDIR=${BKUPDIR}/${NAME}_hotbackup
    DBVOL=`echo $x|cut -d ',' -f2-|tr ',' '|'`
    EXCEPTVOLUMES=${EXCEPTVOLUMES}"|${DBVOL}"
    mkdir ${SAVEDIR}
    docker exec ${NAME} redis-cli --rdb - > ${SAVEDIR}/redisdata.rdb
done

BKUPVOLUMES=''
for x in `docker volume ls --format "{{.Name}}"|grep -vE "(${EXCEPTVOLUMES})"`; do
    BKUPVOLUMES=${BKUPVOLUMES}" -v ${x}:/borg/data/${x}"
done

docker run --rm \
  -e BORG_REPO=/borg/repo \
  -e BACKUP_DIRS=/borg/data \
  -e BORG_PARAMS="break-lock" \
  -e BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes \
  -v ${BORGREPO}:/borg/repo \
  -v ${BKUPVOLUMENAME}:/borg/data \
  ${BKUPVOLUMES} \
  pschiffe/borg

docker run --rm \
  -e ARCHIVE=`date +%Y-%m-%d`\
  -e BORG_REPO=/borg/repo \
  -e BACKUP_DIRS=/borg/data \
  -e COMPRESSION=zstd,9 \
  -e PRUNE=1 \
  -e KEEP_DAILY=30 \
  -e KEEP_WEEKLY=52 \
  -e KEEP_MONTHLY=60 \
  -e BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes \
  -v ${BORGREPO}:/borg/repo \
  -v ${BKUPVOLUMENAME}:/borg/data \
  ${BKUPVOLUMES} \
  pschiffe/borg
