#!/bin/ash

if [ -z "${BORGREPO:-}" ] ; then
    echo 'ERROR: $BORGREPO is required'
    exit 1
fi

if [ -n "${TZ:-}" ] ; then
    cp /usr/share/zoneinfo/${TZ} /etc/localtime
    echo "${TZ}" > /etc/timezone
fi

# exec random minute to spread peak
echo `expr ${RANDOM} % 60`" 2 * * * ash /backup-cron.sh" > /var/spool/cron/crontabs/root

echo "export BKUPVOLUMENAME="`docker inspect ${HOSTNAME} --format "{{.Mounts}}"|sed -E 's@.*volume ([^ ]+) [^ ]+ /tmp/bkup.*@\1@g'` > /.env
echo "export BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes" >> /.env
echo "export BORGREPO=${BORGREPO}" >> /.env

crond -f -L /dev/stderr
