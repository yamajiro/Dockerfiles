#!/bin/sh

export CONTAINER_CMD=${CONTAINER_CMD:-podman}
export DOCKER_BUILDKIT=1

export BUILDCMD="build"
if [ ${CONTAINER_CMD} = "podman" ]; then
    BUILDCMD="buildx build"
fi

export BHOST=${HOST}
if [ ! "x"${BHOST} = "x" ]; then
    BHOST="-H "${BHOST}
fi

export REGISTRY_HOST=${REGISTRY_HOST:-localhost:5000}
for x in `ls -d */`; do
    (
        cd ${x}
        TAG=${REGISTRY_HOST}/${x%/*}
        ${CONTAINER_CMD} ${BHOST} ${BUILDCMD} --pull -t ${TAG} .
        ${CONTAINER_CMD} ${BHOST} push ${TAG}
        ${CONTAINER_CMD} ${BHOST} image rm ${TAG}
    )
done

${CONTAINER_CMD} ${BHOST} image prune -f
